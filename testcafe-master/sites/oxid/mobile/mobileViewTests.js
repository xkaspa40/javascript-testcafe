import page from '../page-model';

fixture`Basic view tests`
  .page`${page.pageURL}`;

test('Show product category list', async (t) => {
  const firstCategoryName = ((await page.menu.mobileCategoryLink.getAttribute('title')).toUpperCase());
  console.log(`Trying to access ${firstCategoryName} category list`);
  await t
    .click(page.menu.menuToggler)
    .click(page.menu.mobileCategoryLink.filterVisible());
  const breadCrumb = ((await page.breadcrumb.nth(-1).innerText).toUpperCase());
  await t
    .expect(breadCrumb)
    .contains(firstCategoryName);
});

test('Show product detail', async (t) => {
  const productName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper)
    .find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to access ${productName} detail`);
  await t
    .click(page.productRotator.toCart
      .parent(page.productRotator.productWrapper)
      .find(page.productRotator.detailLink))
    .expect((page.headings.h1).innerText).contains(productName);
});

test('Show basket page', async (t) => {
  await t
    .click(page.basketBox.basket)
    .expect((page.body.classNames)).match(page.bodyClass.basket, 'Body class did not contained basket class.');
});
test('Show register page', async (t) => {
  await t
    .click(page.accountBox.box.filterVisible())
    .click(page.myAccount.mobileRegisterButton)
    .expect((page.myAccount.registerForm).exists)
    .ok();
});
