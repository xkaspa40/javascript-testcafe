import page from '../page-model';

fixture`Checkout tests`
  .page`${page.pageURL}`
  .beforeEach(async (t) => {
    if (await page.sfBar.exists) {
      await t
        .click(page.sfBar);
    }
    await t
      .click(page.cookiesBoxButton);
  });

test('Get to confirmation/user basket step', async (t) => {
  const productName = await page.productRotator.toCart.parent(page.productRotator.productWrapper)
    .find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to checkout with ${productName}`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .click(page.checkoutPage.nextStepButton)
    .wait(2000)
    .click(page.checkoutPage.nextStepButton)
    .expect(page.body.classNames)
    .match(page.bodyClass.user, 'Body class did not contain user-page keyword');
});

test('Select different shipment and proceed to confirmation/user step', async (t) => {
  const productName = await page.productRotator.toCart.parent(page.productRotator.productWrapper)
    .find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to checkout with ${productName}`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .click(page.checkoutPage.nextStepButton)
    .wait(2000);
  if (await page.checkoutPage.inactiveShippingOption.exists) {
    await t
      .click(page.checkoutPage.inactiveShippingOption);
  }
  if (await page.checkoutPage.balikovnaActive) {
    await t
      .click(page.checkoutPage.balikovnaCitySelector)
      .click(page.checkoutPage.balikovnaCityValue.nth(-1))
      .click(page.checkoutPage.balikovnaOfficeSelector)
      .click(page.checkoutPage.balikovnaOfficeValue.nth(-1));
  }
  await t
    .wait(2000)
    .click(page.checkoutPage.nextStepButton)
    .expect(page.body.classNames)
    .match(page.bodyClass.user, 'Body class did not contain user-page keyword');
});

test('Select different payment and proceed to confirmation/user step', async (t) => {
  const productName = await page.productRotator.toCart.parent(page.productRotator.productWrapper)
    .find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to checkout with ${productName}`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .click(page.checkoutPage.nextStepButton);
  if (await page.checkoutPage.inactivePaymentOption.exists) {
    await t
      .click(page.checkoutPage.inactivePaymentOption);
  }
  await t
    .wait(2000)
    .click(page.checkoutPage.nextStepButton)
    .expect(page.body.classNames).match(page.bodyClass.user, 'Body class did not contain user-page keyword');
});
