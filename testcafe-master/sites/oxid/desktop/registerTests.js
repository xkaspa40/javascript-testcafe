import page from '../page-model';

fixture`Register tests`
  .page`${page.pageURL}`;

const validUserEmail = page.validUser.email;
const invalidUserEmail = page.invalidUser.email;
test('Register Valid User', async (t) => {
  await t
    .click(page.accountBox.loginDropDown)
    .click(page.loginDropDown.registerButton.nth(0))
    .typeText(page.registerFields.name, page.validUser.name)
    .typeText(page.registerFields.lastName, page.validUser.lastName)
    .typeText(page.registerFields.email, validUserEmail)
    .typeText(page.registerFields.password, page.validUser.password)
    .typeText(page.registerFields.phone, page.validUser.phone)
    .typeText(page.invoiceAddressFields.street, page.validAddress.street)
    .typeText(page.invoiceAddressFields.zipCode, page.validAddress.zipCode)
    .typeText(page.invoiceAddressFields.number, page.validAddress.number)
    .typeText(page.invoiceAddressFields.city, page.validAddress.city);
  if (await page.registerFields.shipAddr.filter('h5:not(.collapsed)').exists) {
    await t
      .click(page.registerFields.shipAddr);
  }
  await t
    .click(page.myAccount.nextButton)
    .expect(page.accountBox.box.classNames)
    .notContains(page.accountBox.notLoggedInClass);
});
test('Log in valid user', async (t) => {
  await t
    .click(page.accountBox.loginDropDown)
    .expect(page.accountBox.loginDropDown.exists).ok()
    .typeText(page.loginDropDown.email.filterVisible(), validUserEmail)
    .typeText(page.loginDropDown.password.filterVisible(), page.validUser.password)
    .click(page.loginDropDown.loginButton.filterVisible())
    .expect(page.accountBox.box.classNames)
    .notContains(page.accountBox.notLoggedInClass);
});
test('Fail to register invalid User', async (t) => {
  await t
    .click(page.accountBox.loginDropDown)
    .click(page.loginDropDown.registerButton.nth(0))
    .typeText(page.registerFields.name, page.invalidUser.name)
    .typeText(page.registerFields.lastName, page.invalidUser.lastName)
    .typeText(page.registerFields.email, invalidUserEmail)
    .typeText(page.registerFields.password, page.invalidUser.password)
    .typeText(page.registerFields.phone, page.invalidUser.phone)
    .typeText(page.invoiceAddressFields.street, page.validAddress.street)
    .typeText(page.invoiceAddressFields.zipCode, page.validAddress.zipCode)
    .typeText(page.invoiceAddressFields.number, page.validAddress.number)
    .typeText(page.invoiceAddressFields.city, page.validAddress.city);
  if (await page.registerFields.shipAddr.filter('h5:not(.collapsed)').exists) {
    await t
      .click(page.registerFields.shipAddr);
  }
  await t
    .click(page.myAccount.nextButton)
    .expect(page.accountBox.box.classNames)
    .contains(page.accountBox.notLoggedInClass);
});
test('Fail to log in invalid user', async (t) => {
  await t
    .click(page.accountBox.loginDropDown)
    .expect(page.accountBox.loginDropDown.exists).ok()
    .typeText(page.loginDropDown.email.filterVisible(), invalidUserEmail)
    .typeText(page.loginDropDown.password.filterVisible(), page.invalidUser.password)
    .click(page.loginDropDown.loginButton.filterVisible())
    .wait(3000)
    .expect(page.accountBox.box.classNames)
    .contains(page.accountBox.notLoggedInClass);
});
