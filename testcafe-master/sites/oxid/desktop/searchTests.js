import page from '../page-model';
import { Selector } from 'testcafe';

fixture`Search functionality tests`
  .page`${page.pageURL}`;

test('Search for product', async (t) => {
  const productName = await Selector(page.productRotator.title).getAttribute('title');
  console.log(`Trying to search for ${productName}`);
  await t
    .typeText(page.search.input, productName)
    .click(page.search.button)
    .expect(page.body.classNames)
    .match(page.bodyClass.search, 'Body class did not contain search keyword')
    .expect(page.productBox.title.getAttribute('title'))
    .contains(productName);
});
test('Autocomplete product', async (t) => {
  const productName = await Selector(page.productRotator.title).getAttribute('title');
  const autoCompleteName = page.getValidAutocompleteString(productName);
  autoCompleteName.substr(0, 25);
  const autocompleteResultLabel = page.search.autocomplete
    .find(page.search.autocompleteLabel).innerText;
  await t
    .typeText(page.search.input, autoCompleteName)
    .expect((page.search.autocomplete).exists).ok()
    .expect(autocompleteResultLabel)
    .contains(autoCompleteName)
    .click((page.search.autocomplete).find(page.search.autocompleteLabel))
    .expect(page.body.classNames)
    .match(page.bodyClass.detail, 'Body class did not contain details-page keyword');
});
