import page from '../page-model';

fixture`Basket tests`
  .page`${page.pageURL}`;

test('Add product to basket from detail', async (t) => {
  const testedProductName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper).find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to put product: ${testedProductName} to basket...`);
  await t
    .click(page.productRotator.toCart
      .parent(page.productRotator.productWrapper).find(page.productRotator.detailLink))
    .click(page.productDetail.toBasketButton)
    .click(page.addedToBasketPopUp.toBasketButton)
    .expect(page.basketPage.productTitle.textContent)
    .contains(testedProductName);
});

test('Add product to basket from category', async (t) => {
  await t
    .click(page.menu.categoryLink);
  const testedProductName = await page.productBox.title.getAttribute('title');
  console.log(`Trying to put product: ${testedProductName} to basket...`);
  await t
    .click(page.productBox.toBasketButton)
    .expect(page.basketBox.basketItemCount.textContent)
    .notEql('0');
});

test('Find product on basket page', async (t) => {
  const testedProductName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper).find(page.productRotator.title).getAttribute('title');
  console.log(`Looking for product: ${testedProductName} on basket page...`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .expect(page.basketPage.productTitle.textContent)
    .contains(testedProductName);
});

test('Increase product count on basket page ', async (t) => {
  const testedProductName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper).find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to increase product count for product: ${testedProductName} on basket page...`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .expect(page.basketPage.productTitle.textContent)
    .contains(testedProductName)
    .typeText(page.basketPage.itemCountInput.filterVisible(), '2', { replace: true })
    .expect(page.basketPage.itemCountInput.value)
    .eql('2');
});

test('Delete item by decreasing counter', async (t) => {
  const testedProductName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper).find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to add to basket and then remove product: ${testedProductName} by decreasing counter on basket page...`);
  await t
    .click(page.productRotator.detailLink)
    .click(page.productDetail.toBasketButton)
    .click(page.addedToBasketPopUp.toBasketButton)
    .expect(page.basketPage.productTitle.textContent)
    .contains(testedProductName)
    .click(page.basketPage.spinnerDown)
    .expect(page.basketPage.productTitle.withText(testedProductName).exists)
    .notOk();
});

test('Delete item by setting counter to zero', async (t) => {
  const testedProductName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper).find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to add to basket and then remove product: ${testedProductName} with counter on basket page...`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .expect(page.basketPage.productTitle.textContent)
    .contains(testedProductName)
    .typeText(page.basketPage.itemCountInput.filterVisible(), '0', { replace: true })
    .pressKey('enter')
    .expect(page.basketPage.productTitle.withText(testedProductName).exists)
    .notOk();
});

test('Delete item by clicking remove button', async (t) => {
  const testedProductName = await page.productRotator.toCart
    .parent(page.productRotator.productWrapper).find(page.productRotator.title).getAttribute('title');
  console.log(`Trying to add to basket and then remove product: ${testedProductName} with button on basket page...`);
  await t
    .click(page.productRotator.toCart)
    .click(page.addedToBasketPopUp.toBasketButton)
    .expect(page.basketPage.productTitle.textContent)
    .contains(testedProductName)
    .click(page.basketPage.removeItemButton.filterVisible())
    .expect(page.basketPage.productTitle.withText(testedProductName).exists)
    .notOk();
});
