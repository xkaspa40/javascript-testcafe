import { Selector } from 'testcafe';

class Page {
  constructor() {
    this.pageURL = 'http://oxid.test.oxyshop.cz/';
    this.listProduct = Selector('.product');
    this.modalClose = Selector('.modal-dialog .close');
    this.modalReminder = Selector('.modal');
    this.modalreminderClose = Selector('.modal .js-remind-close');
    this.categoryLink = Selector('#content .category-items a');
    this.cookiesBoxButton = Selector('#cookies-box .btn-primary');
    this.sfBar = Selector('.sf-toolbar .hide-button');
    this.mobileCategoryLink = Selector('#content .category-group .next');
    this.search = {
      searchToggler: Selector('#js-respo-search'),
      searchModal: Selector('#search-input-modal'),
      input: Selector('.input-group.js-input-group'),
      button: Selector('#searchGo'),
      mobileInput: Selector('#responsive-search .txt'),
      mobileButton: Selector('#responsive-search #box-search-sm'),
      autocomplete: Selector('.search-autocomplete'),
      autocompleteLabel: '.autocomplete-label',
    };
    this.menu = {
      menuToggler: Selector('#js-respo-hamburger'),
      categoryLink: Selector('#js-header-menu .nav-item'),
      mobileCategoryLink: Selector('#js-main-menu .nav-item a'),
    };
    this.validUser = {
      name: 'Valid',
      lastName: 'TestCafe',
      email: `testcafe${this.getValidFriendlyDateTime()}@gmail.com`,
      password: 'blamebuchta',
      phone: '800123456',
    };
    this.validAddress = {
      street: 'Londýnské Náměstí',
      number: '6',
      zipCode: '63900',
      city: 'Brno',
      country: 'Česká republika',
    };
    this.invalidUser = {
      name: 'Invalid',
      lastName: 'testCafe',
      email: `testcafe${this.getValidFriendlyDateTime()}gmail.com`,
      password: 'blame',
      phone: 'notANumber',
    };
    this.registerFields = {
      name: Selector('#contact_oxuser__oxfname'),
      lastName: Selector('#contact_oxuser__oxlname'),
      email: Selector('#contact_oxuser__oxusername'),
      phone: Selector('#contact_oxuser__oxfon'),
      password: Selector('#lgn_pwd'),
      shipAddr: Selector('#user-shipadr-header'),
    };
    this.loginPage = {
      header: Selector('#LoginHeader'),
      email: Selector('#LoginEmail'),
      password: Selector('#LoginPwd'),
      loginButton: Selector('#Login'),
    };
    this.invoiceAddressFields = {
      street: Selector('#billingaddress_oxuser__oxstreet'),
      number: Selector('#billingaddress_oxuser__oxstreetnr'),
      zipCode: Selector('#billingaddress_oxuser__oxzip'),
      city: Selector('#billingaddress_oxuser__oxcity'),
      country: Selector('#inv_country_select'),
    };
    this.body = Selector('body');
    this.headings = {
      h1: Selector('h1'),
    };
    this.branch = {
      toggler: Selector('#header .changebranch'),
      mobileToggler: Selector('.changebranch'),
      selector: Selector('#branch'),
      option: 'option',
      submit: Selector('#branchModal .choose-branch .btn-primary'),
    };
    this.accountBox = {
      box: Selector('.header-account .header-link'),
      notLoggedInClass: 'user',
      registerButton: Selector('.register.btn'),
      loginButton: Selector('#header .header-login'),
      mobileLoginButton: Selector('.header-account .header-link'),
      loginDropDown: Selector('.header-account .dropdown-toggle'),
    };
    this.loginDropDown = {
      email: Selector('#header-login-mail'),
      password: Selector('#header-login-pswd'),
      loginButton: Selector('.header-account .btn-primary'),
      registerButton: Selector('.account-reg li'),
    };
    this.basketBox = {
      basketItemCount: Selector('.header-basket .count'),
      basket: Selector('.header-middle .header-basket'),
    };
    this.bodyClass = {
      user: /user-page/,
      search: /search/,
      detail: /details-page/,
      basket: /basket-page/,
      payment: 'payment-page',
    };
    this.breadcrumb = Selector('.breadcrumbs li a span');
    this.myAccount = {
      nextButton: Selector('.prevnext button'),
      registerForm: Selector('.form.user-contact-box'),
      mobileRegisterButton: Selector('#LoginRegister'),

    };
    this.productBox = {
      wrapper: Selector('.product-wrap'),
      title: Selector('.product .title a'),
      detailLink: Selector('.product a'),
      toBasketButton: Selector('.product .btn-to-cart'),
    };
    this.productRotator = {
      productWrapper: ('.slick-active .product'),
      title: ('.slick-active .product .title a'),
      detailLink: ('.slick-active .product a'),
      toCart: Selector('.slick-active .product .btn.btn-to-cart'),
    };
    this.productDetail = {
      toBasketButton: Selector('.amopricebox .btn.tocart'),
    };
    this.addedToBasketPopUp = {
      toBasketButton: Selector('.basket-popup-tocart'),
      closePopUp: Selector('.basket-popup-continue'),
      title: Selector('#popupAddedTitle'),
      errorClass: 'popup-error-title',
    };
    this.basketPage = {
      productTitle: Selector('#basketsummary .art_title a'),
      basketHeader: Selector('h1.baskethead'),
      productWrapper: '.itemrow',
      spinnerInput: Selector('.ui-spinner-input'),
      spinnerUp: Selector('.ui-spinner-up'),
      spinnerDown: Selector('.ui-spinner-down'),
      itemCountInput: Selector('.amount input.form-control'),
      removeItemButton: Selector('.item-remove .removebtn'),
    };
    this.checkoutPage = {
      nextStepButton: Selector('.prevnext .arrowright .btn-primary'),
      inactiveShippingOption: Selector('.payment-shipping .item-row input:not(:checked)'),
      inactivePaymentOption: Selector('.payment-method .item-row input:not(:checked)'),
      balikovnaActive: Selector('oxidbalikovna.active'),
      balikovnaCitySelector: Selector('.selection #select2-balikovna-city-container'),
      balikovnaCityValue: Selector('#select2-balikovna-city-results .select2-results__option'),
      balikovnaOfficeSelector: Selector('.selection #select2-balikovna-office-container'),
      balikovnaOfficeValue: Selector('#select2-balikovna-office-results .select2-results__option'),
      ulozenkaActive: Selector('.oxidulozenka.active'),
      ulozenkaRegionSelector: Selector('.selection #select2-ulozenka-region-container'),
      ulozenkaRegionValue: Selector('#select2-ulozenka-region-results .select2-results__option'),
      ulozenkaBranchSelector: Selector('.selection #select2-ulozenka-branch-container'),
      ulozenkaBranchvalue: Selector('#select2-ulozenka-branch-results .select2-results__option'),
      personalCollectionBranch: Selector('#branchSelect'),
      agmoActive: Selector('.oxagmo.active'),
      agmoOption: Selector('.agmo-option'),
    };
  }

  getValidFriendlyDateTime() {
    const currentDate = new Date();
    const minutes = currentDate.getMinutes();
    const hours = currentDate.getHours();
    const day = currentDate.getDate();
    const month = currentDate.getMonth();
    const year = currentDate.getFullYear();
    return `${year}/${day}/${month}|${hours}-${minutes}`;
  }

  getValidAutocompleteString(autocompleteString) {
    const shortAutocomplete = autocompleteString.substr(0, 25);
    const parts = shortAutocomplete.split(' ');
    let validAutocomplete = '';
    for (const part of parts) {
      if (part.length < 3) {
        return validAutocomplete;
      }
      validAutocomplete += `${part} `;
    }
    return validAutocomplete;
  }
}

export default new Page();
