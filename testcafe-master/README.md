## TestCafé tests

This repository contains TestCafé testing suite with tests for specific websites.

## Installation
Install yarn if you don't have it https://yarnpkg.com/en/docs/install

Run `yarn global add testcafe` to install testcafe globally so the command `testcafe` works anywhere.
## How to run tests
https://devexpress.github.io/testcafe/documentation/using-testcafe/

TLDR:

Clone this repository and in top folder run:

`testcafe chrome sites/tivisx5/` to run all tests on tivisx5 site

`testcafe -c 3 "chrome:emulation:device=iphone X" sites/tivisx5/` to run all tivisx5 tests in 3 concurrent Chrome instances in iPhone X device emulation mode
 
 `testcafe firefox:headless sites/tivisx5/desktop/viewTests.js` to run specific test file in Firefox headless mode
